const express = require('express');
const port = 3000;
const usersRouter = require('./Routes/users');
const contactRouter = require('./Routes/contact'); 

const app = express();

const mongoose = require('mongoose');
const url = 'mongodb+srv://root:root@cluster0.ndaqs.mongodb.net/?retryWrites=true&w=majority';
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};
mongoose.connect(url, options)
.then(console.log('connecting'))
.catch(err => console.log(`error: ${err}`));
mongoose.connection.on('error',(err)=>{
console.log("Erro:" + err);
});
mongoose.connection.on('disconnected',()=>{
console.log("Aplicação desconectada");
});




app.use(express.json())



app.use('/usuario', usersRouter)

app.use('/contato', contactRouter)





app.listen(port,()=>{
    console.log("-----------------------------------")
    console.log(`Rodando na porta ${port}`)
    console.log("-----------------------------------")
})

