// const express = require('express');
// const router = express.Router();

// router.get('/',(req, res) =>{
//     res.send("MÉTODO GET USERS")
// })

// router.post('/',(req, res) =>{
//     res.send("MÉTODO POST USERS")
// })
// module.exports = router;

const express = require('express');
const router = express.Router();
const Users = require('../model/user');

router.get('/', (req, res) => {
Users.find({},(err,data)=>{
    if(err) return res.send({ message:`Erro na consulta de usuários!`});

    return res.send(data);
    });
});

module.exports = router;